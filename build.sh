#!/usr/bin/env bash

# Write all image metadata in the docker format, not the standard OCI format.
# Newer versions of docker can handle the OCI format, but older versions, like
# the one shipped with Fedora 30, cannot handle the format.
export BUILDAH_FORMAT=docker

#set -o errexit
tag=$1

test -z "$tag" && exit 1

# Determine which image we base our own on.
from_tag=docker.io/php:7.4-fpm

# Use some environment variables here so we can provide them in CI pipelines.
image_name=phppgadmin
phppgadmin_version=7.12.1
phppgadmin_url="https://github.com/phppgadmin/phppgadmin/releases/download/REL_7-12-1/phpPgAdmin-$phppgadmin_version.tar.bz2"
container=$(buildah from $from_tag)
registry_image=${REGISTRY_IMAGE:-registry.hub.docker.com/stemid/$image_name}
registry_tag=${REGISTRY_TAG:-$phppgadmin_version-$tag}

buildah config --label maintainer="Stefan Midjich <swehack at gmail.com>" $container
#buildah config --entrypoint /bin/bash $container

# Download phppgadmin
test -d "phpPgAdmin-$phppgadmin_version" || \
  curl -sLo - "$phppgadmin_url" | \
  tar -xvjf - -C .
buildah copy $container "phpPgAdmin-$phppgadmin_version/"* /var/www/html/

buildah run $container apt update

echo "$0: buildah commit $image_name:$registry_tag"
buildah commit $container $image_name:$registry_tag

echo "$0: buildah push to docker://$registry_image:$registry_tag"
#buildah push --authfile ./auth.json localhost/$image_name:$registry_tag docker://$registry_image:$registry_tag
